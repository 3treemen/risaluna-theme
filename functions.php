<?php
/**
 * Risaluna-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package risaluna-theme
 */

add_action( 'wp_enqueue_scripts', 'xclean_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function xclean_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'xclean-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'risaluna-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'xclean-style' )
	);

}
